using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel.Client;
using Microsoft.AspNetCore.Mvc;

namespace CrewDragonApi.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public HomeController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        [Route("/")]
        public async Task<IActionResult> Index()
        {
            //retrieve access_token
            var serverClient = _httpClientFactory.CreateClient();
            
            var discoveryDocument = await serverClient.GetDiscoveryDocumentAsync("https://localhost:5000/"); //now we know everything what we need about our identity server

            var tokenResponse = await serverClient.RequestClientCredentialsTokenAsync(
                new ClientCredentialsTokenRequest
                {
                    Address = discoveryDocument.TokenEndpoint,
                    ClientId = "CrewDragonApi",
                    ClientSecret = "spacexRulesTheWorld",
                    Scope = "CanaveralApi"
                });
            
            //retrieve secret data
            var canaveralClient = _httpClientFactory.CreateClient();
            
            canaveralClient.SetBearerToken(tokenResponse.AccessToken);

            var response = await canaveralClient.GetAsync("http://localhost:8000/secret");

            var content = await response.Content.ReadAsStringAsync();
            
            return Ok(new
            {
                access_token = tokenResponse.AccessToken,
                msg = content
            });
        }
    }
}