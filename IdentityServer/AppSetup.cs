using System.Collections;
using System.Collections.Generic;
using IdentityModel;
using IdentityServer4.Models;

namespace IdentityServer
{
    public static class AppSetup
    {
        //how IS4 will know about my API 
        public static IEnumerable<ApiResource> GetApis() => 
            new List<ApiResource>
            {
                new ApiResource
                {
                    Name = "CanaveralApi",
                    Scopes = new List<string> { "CanaveralApi" },
                }
            };

        public static IEnumerable<IdentityResource> GetIdentity() =>
            new List<IdentityResource>
            {
                new IdentityResource(
                    name: "profile",
                    userClaims: new[] {"name", "email", "website"},
                    displayName: "Your profile data")
            };

        public static IEnumerable<ApiScope> GetScopes() =>
            new List<ApiScope>
            {
                new ApiScope(name: "CanaveralApi")
            };
        
        public static IEnumerable<Client> GetClients() =>
            new List<Client>
            {
                new Client
                {
                    ClientId = "CrewDragonApi",
                    ClientSecrets = { new Secret("spacexRulesTheWorld".ToSha256())},
                    AllowedGrantTypes = GrantTypes.ClientCredentials, //how it will be retrieving an access token
                    AllowedScopes = {"CanaveralApi"} //what can be done after getting access token
                }
            };
    }
}