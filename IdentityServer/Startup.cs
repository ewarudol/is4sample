using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer4.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

// *** IdentityServer4 is an OpenID Connect and OAuth 2.0 framework for ASP.NET Core.
// https://identityserver4.readthedocs.io/en/latest/
// Authentication as a Service
// Single Sign-on / Sign-out
// Access Control for APIs
// Federation Gateway (like Azure Active Directory, Google, Facebook)
// Focus on Customization
// Mature Open Source

namespace IdentityServer
{
    //https://localhost:5000/.well-known/openid-configuration
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddIdentityServer()
                .AddInMemoryIdentityResources(AppSetup.GetIdentity())
                .AddInMemoryApiResources(AppSetup.GetApis())
                .AddInMemoryClients(AppSetup.GetClients())
                .AddInMemoryApiScopes(AppSetup.GetScopes())
                .AddDeveloperSigningCredential(); //adds cert for signing tokens
            
            services.AddControllersWithViews();
        }
        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseIdentityServer();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });
        }
    }
}