using System.ComponentModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CanaveralApi.Controllers
{
    public class SecretController : Controller
    {
        [Authorize]
        [Route("/secret")]
        public string Index()
        {
            return "Canaveral Api restricted channel only for astronauts!";
        }
    }
}