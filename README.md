# IdentityServer4 training

## Info
- This repository is under development only for educational purposes, it can contain raw code, too many comments and mistakes/errors
- Ideas, implementation and knowledge from https://invidious.snopyta.org/watch?v=Fhfvbl_KbWo

## Before build
Make sure you have a trusted certificate on your machine, because IS4 server 
requires https connection. Of course you can try to convince all client apps to
allow untrusted certs, but this way will be painful. 

In case of Windows OS you can generate certificate during first browser launch of
your webpage or with package "dotnet dev-certs". In case of Linux OS dotnet certificates 
are stored in places dependent on distro thus above solutions are not available. 

For Manjaro Linux certs path is here: "$HOME/.aspnet/https/". 
You can use that splendid project: https://github.com/amadoa/dotnet-devcert-linux
to generate new cert in the desired path and then everything should works properly.

## Content
- base setup of the server